#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QMainWindow>

namespace Ui {
class MainView;
}

class MainView : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainView(QWidget *parent = nullptr);
    ~MainView();

private slots:
    void on_btnAstroid_clicked();

    void on_btnCycloid_clicked();

    void on_btnHyugns_clicked();

    void on_btmHypo_clicked();

    void on_btnLine_clicked();

private:
    Ui::MainView *ui;
};

#endif // MAINVIEW_H
