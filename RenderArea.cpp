#include "RenderArea.h"
#include <QPaintEvent>
#include <QPainter>
#define _USE_MATH_DEFINES
#include <math.h>

RenderArea::RenderArea(QWidget *parent) :
    QWidget(parent),
    mShapeColor(QColor(255,255,255)),
    mBackgroundColor(QColor(0,0,255)),
    mShape(Astroid)
{
    on_shape_changed();

}

void RenderArea::setBackgroundColor(QColor color)
{
    mBackgroundColor = color;
}

QSize RenderArea::minimumSizeHint() const
{
    return QSize(100,100);
}

QSize RenderArea::sizeHint() const
{
    return QSize(400,200);
}


void RenderArea::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);




    painter.setBrush(mBackgroundColor);
    painter.setPen(mShapeColor);

    painter.drawRect(this->rect());

    QPoint center = this->rect().center();


    float step = mIntervalLength / mStepCount;

    for (float t = 0; t  < mIntervalLength; t += step) {
        QPointF point = compute(t);
        QPoint pixel;
        pixel.setX(point.x() * mScale + center.x());
        pixel.setY(point.y() * mScale + center.y());

        painter.drawPoint(pixel);
    }

}

QPointF RenderArea::compute_astroid(float t)
{
    float cos_t = cos(t);
    float sin_t = sin(t);

    float X = 2 * cos_t * cos_t * cos_t;
    float Y = 2 * sin_t * sin_t * sin_t;
    return QPointF(X, Y);
}



QPointF RenderArea::compute_cycloid(float t)
{
    return QPointF(
                    1.5 * (1 - cos(t)), // X
                    1.5 * (t - sin(t))  // Y
                );
}

QPointF RenderArea::compute_huygens(float t)
{
    return QPointF(
                    4 * (3 * cos(t) - cos(3 * t)), // X
                    4 * (3 * sin(t) - sin(3 * t))  // Y
                );
}

QPointF RenderArea::compute_hypo(float t)
{
    return QPointF(
                    1.5 * (2 * cos(t) + cos(2*t)), // X
                    1.5 * (2 * sin(t) - sin(2*t))  // Y
                );
}

QPointF RenderArea::compute_Line(float t)
{
    return QPointF(
                    (1 - t), // X
                    (1 - t)  // Y
                );
}

void RenderArea::on_shape_changed()
{
    switch (mShape) {

    case RenderArea::Astroid:
        mScale = 40.0f;
        mIntervalLength = 2 * M_PI;
        mStepCount = 256;
        break;

    case RenderArea::Cycloid:
        mScale = 4;
        mIntervalLength = 6 * M_PI;
        mStepCount = 128;
        break;

    case RenderArea::HuygensCycloid:
        mScale = 4;
        mIntervalLength = 4 * M_PI;
        mStepCount = 256;
        break;

    case RenderArea::HypoCycloid:
        mScale = 15;
        mIntervalLength = 2 * M_PI;
        mStepCount = 256;
        break;
    case RenderArea::Line:
        mScale = 50;
        mIntervalLength = 1;
        mStepCount = 256;
        break;

    default:
        break;
    }
}

QPointF RenderArea::compute(float t)
{
    switch (mShape) {
    case RenderArea::Astroid:
        return compute_astroid(t);
        break;

    case RenderArea::Cycloid:
        return compute_cycloid(t);
        break;

    case RenderArea::HuygensCycloid:
        return compute_huygens(t);
        break;

    case RenderArea::HypoCycloid:
        return compute_hypo(t);
        break;

    case RenderArea::Line:
        return compute_Line(t);
        break;

    default:
        break;
    }

    return QPointF();
}

RenderArea::ShapeType RenderArea::shape() const
{
    return mShape;
}

void RenderArea::setShape(const ShapeType &shape)
{
    mShape = shape;
    on_shape_changed();
}

QColor RenderArea::backgroundColor() const
{
    return mBackgroundColor;
}
