#include "MainView.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainView w;
    w.show();

    return app.exec();
}
