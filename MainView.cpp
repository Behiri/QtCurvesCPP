#include "MainView.h"
#include "ui_MainView.h"

MainView::MainView(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainView)
{
    ui->setupUi(this);
}

MainView::~MainView()
{
    delete ui;
}

void MainView::on_btnAstroid_clicked()
{
    this->ui->renderArea->setShape(RenderArea::Astroid);
    emit this->ui->renderArea->repaint();
}

void MainView::on_btnCycloid_clicked()
{
    this->ui->renderArea->setShape(RenderArea::Cycloid);
    emit this->ui->renderArea->repaint();
}

void MainView::on_btnHyugns_clicked()
{
    this->ui->renderArea->setShape(RenderArea::HuygensCycloid);
    emit this->ui->renderArea->repaint();
}

void MainView::on_btmHypo_clicked()
{
    this->ui->renderArea->setShape(RenderArea::HypoCycloid);
    emit this->ui->renderArea->repaint();
}

void MainView::on_btnLine_clicked()
{
    this->ui->renderArea->setShape(RenderArea::Line);
    emit this->ui->renderArea->repaint();
}
