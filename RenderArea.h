#ifndef RENDERAREA_H
#define RENDERAREA_H

#include <QWidget>
#include <QColor>

class RenderArea : public QWidget
{
    Q_OBJECT
public:
    explicit RenderArea(QWidget *parent = nullptr);

    enum ShapeType {Astroid, Cycloid, HuygensCycloid, HypoCycloid, Line};



signals:

public slots:

    // QWidget interface
public:
    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;

    // QWidget interface
    void setBackgroundColor(QColor color);
    QColor backgroundColor() const;

    ShapeType shape() const;
    void setShape(const ShapeType &shape);

protected:
    void paintEvent(QPaintEvent *event) override;
private:
    QPointF compute_astroid(float t);
    QPointF compute(float t);
    QPointF compute_cycloid(float t);
    QPointF compute_huygens(float t);
    QPointF compute_hypo(float t);
    QPointF compute_Line(float t);
    void on_shape_changed();

private:
    QColor mShapeColor;
    QColor mBackgroundColor;
    ShapeType mShape;

    float mIntervalLength;
    float mScale;
    int mStepCount;
};

#endif // RENDERAREA_H
